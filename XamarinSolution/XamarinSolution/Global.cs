﻿using System;
using System.Collections.Generic;
using System.Text;
using FoodsNGoodsPCL.Model;

namespace XamarinSolution
{
   public class Global
    {
        public static Narucioci_Result logiraniNarucilac { get; set; }
        public static List<Restorani_Result> logiraniNarucilacOmiljeniRestorani { get; set; } = new List<Restorani_Result>();
    }
}
