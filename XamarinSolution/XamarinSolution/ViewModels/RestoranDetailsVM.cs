﻿using FoodsNGoodsPCL.Model;
using FoodsNGoodsPCL.Util;
using System;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Windows.Input;
using Xamarin.Forms;

namespace XamarinSolution.ViewModels
{
    public class RestoranDetailsVM : INotifyPropertyChanged
    {
        private WebAPIHelper restoraniService = new WebAPIHelper("Restorani");
        public event PropertyChangedEventHandler PropertyChanged;

        public int MinimalnaCijenaNarudžbe { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public string Slika { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }
        public string BlokGradNaziv { get; set; }
        public string VlasnikNaziv { get; set; }
        public string WebUrl { get; set; }
        public int RestoranID { get; set; }
        public string AdresaFullNaziv { get; set; }
        public bool isFavoriteRestoran { get; set; } = false;
        public bool IsFavoriteRestoran
        {
            get { return isFavoriteRestoran; }
            set
            {
                isFavoriteRestoran = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("toggleLikeRestoranLabel"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("favouriteImageSource"));
            }
        }
        public bool showLoginError { get; set; } = false;
        public string favouriteImageSource => isFavoriteRestoran ? "heart_red.png" : "heart_dark.png";
        public string toggleLikeRestoranLabel { get; set; } = "";
        public string loginError { get; set; } = "";
        public string LoginError
            {
            get{return loginError;}
            set {
                loginError = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("loginError"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("showLoginError"));
            }
            }

        public string likedByCount { get; set; } = "";
        



        public ICommand ToggleLikeRestoranCommand { get; set; }
        private async void ToggleLikeRestoraniFunc()
        {
            if (Global.logiraniNarucilac != null)
            {
                HttpResponseMessage response = await restoraniService.PostResponse(Global.logiraniNarucilac.KorisnikID+"/"+"toggleLikeRestoran", RestoranID.ToString());
                if (response.IsSuccessStatusCode)
                {
                    IsFavoriteRestoran = !IsFavoriteRestoran;
                    toggleLikeRestoranLabel = isFavoriteRestoran ? "Added to favorite!" : "Removed from favorite!";
                }
                
            }
            else
            {
                LoginError = "To perform action, user must be logged in.";
                showLoginError = true;
            }

        }


        public RestoranDetailsVM(Restorani_Result restoran)
        {
            RestoranID = restoran.RestoranID;
            Naziv = restoran.Naziv;
            Opis = restoran.Opis;
            Slika = restoran.Slika;
            Telefon = restoran.Telefon;
            BlokGradNaziv = restoran.BlokGradNaziv;
            AdresaFullNaziv = restoran.AdresaFullNaziv;
            VlasnikNaziv = restoran.VlasnikNaziv;
            WebUrl = restoran.WebUrl;
            Email = restoran.Email;
            MinimalnaCijenaNarudžbe = restoran.MinimalnaCijenaNarudžbe;
            isFavoriteRestoran = Global.logiraniNarucilacOmiljeniRestorani.Select(x => x.RestoranID).Contains(restoran.RestoranID);
            loginError = "";
            showLoginError = false;


            ToggleLikeRestoranCommand = new Command(ToggleLikeRestoraniFunc);
        }





    }
}
