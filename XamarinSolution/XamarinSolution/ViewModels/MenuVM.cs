﻿using FoodsNGoodsPCL.Model;
using FoodsNGoodsPCL.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Text;
using System.Threading;
using Xamarin.Forms;

namespace XamarinSolution.ViewModels
{
    public class MenuVM : INotifyPropertyChanged
    {
        WebAPIHelper proizvodiService = new WebAPIHelper("Proizvodi");
        public event PropertyChangedEventHandler PropertyChanged;
        Restorani_Result restoran;

        public int HranaID { get; set; }
        public double Cijena { get; set; }
        public int JelovnikID { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public string Prilog { get; set; }
        public int Sifra { get; set; }
        public string Slika { get; set; }
        public int TipKuhinjeID { get; set; }
        public bool indicatorRunning { get; set; } 
        public bool IndicatorRunning
        {
            get { return indicatorRunning; }
            set { indicatorRunning = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("indicatorRunning")); }
        }

        ObservableCollection<Proizvodi_Result> menuItemsList { get; set; }
        public ObservableCollection<Proizvodi_Result> MenuItemsList
        {
            get { return menuItemsList; }
            set { menuItemsList = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("menuItemsList")); }
        }

        public MenuVM(Restorani_Result restoran)
        {
            this.restoran = restoran;
            menuItemsList = new ObservableCollection<Proizvodi_Result>();
            LoadMenuItems();

        }

        private async void LoadMenuItems()
        {
            IndicatorRunning = true;
            HttpResponseMessage response = await proizvodiService.GetResponse("GetProizvodiByRestoran", restoran.RestoranID.ToString());

            if (response.IsSuccessStatusCode)
            {
                MenuItemsList = JsonConvert.DeserializeObject<ObservableCollection<Proizvodi_Result>>(response.Content.ReadAsStringAsync().Result);                
            }
            else
            {
                //ispisati poruku da nema proizvoda za taj restoran
            }
            IndicatorRunning = false;

        }
    }
}
