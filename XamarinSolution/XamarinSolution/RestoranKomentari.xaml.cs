﻿using FoodsNGoodsPCL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinSolution
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RestoranKomentari : ContentPage
	{
        Restorani_Result restoran;
        public RestoranKomentari(Restorani_Result restoran)
        {
            InitializeComponent();
            this.restoran = restoran;
            Title = "Comments" + " - " + restoran.Naziv;
        }
    }
}