﻿using FoodsNGoodsPCL.Model;
using FoodsNGoodsPCL.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinSolution
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RestoraniPage : ContentPage
	{
        WebAPIHelper restoraniService = new WebAPIHelper("Restorani");
		public RestoraniPage ()
		{
			InitializeComponent ();
            LoadData();
		}

        private async void LoadData()
        {
            activityIndicator.IsRunning = true;
            activityIndicator.IsVisible = true;
            HttpResponseMessage response = await restoraniService.GetResponse();
            if(response.IsSuccessStatusCode)
            {
                var restoraniResult = JsonConvert.DeserializeObject<List<Restorani_Result>>(response.Content.ReadAsStringAsync().Result);
                listViewRestorani.ItemsSource = restoraniResult;
            }
            activityIndicator.IsRunning = false;
            activityIndicator.IsVisible = false;
        }

        private void ListViewRestorani_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if(sender is ListView lv)
            {
                lv.SelectedItem = null;
            }
            Navigation.PushAsync(new RestoranDetails((Restorani_Result)e.Item));
        }

        private void SelectedRestMenu_Clicked(object sender, EventArgs e)
        {
            Button clickedButton = sender as Button;
            var selectedItem = clickedButton.CommandParameter as Restorani_Result;

            Navigation.PushAsync(new RestoranMenu(selectedItem));
        }
    }
}