﻿using FoodsNGoodsPCL.Model;
using FoodsNGoodsPCL.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSolution.ViewModels;

namespace XamarinSolution
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RestoranMenu : ContentPage
	{
        WebAPIHelper proizvodiService = new WebAPIHelper("Proizvodi");
        Restorani_Result restoran;
        MenuVM viewmodel;
		public RestoranMenu (Restorani_Result restoran)
		{
			InitializeComponent ();
            viewmodel = new MenuVM(restoran);
            BindingContext = viewmodel;
            this.restoran = restoran;
            Title = "Menu" + " - " + restoran.Naziv;
		}

        protected override void OnAppearing()
        {         
            base.OnAppearing();
        }

        private void ListViewMenu_ItemTapped(object sender, ItemTappedEventArgs e)
        {

        }
    }
}   