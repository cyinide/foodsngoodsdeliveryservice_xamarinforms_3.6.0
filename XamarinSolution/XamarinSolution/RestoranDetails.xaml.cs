﻿using FoodsNGoodsPCL.Model;
using FoodsNGoodsPCL.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinSolution.ViewModels;

namespace XamarinSolution
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RestoranDetails : ContentPage
	{
        WebAPIHelper restoraniService = new WebAPIHelper("Restorani");

        private RestoranDetailsVM viewmodel;
        private Restorani_Result restoran;


        public RestoranDetails (Restorani_Result restoran)
		{
			InitializeComponent ();
            viewmodel = new RestoranDetailsVM(restoran);
            Title = "Restaurant" + " - " + restoran.Naziv;
            BindingContext = viewmodel;
            this.restoran = restoran;   

		}

        protected override void OnAppearing()
        {
            GetLikesForRestaurant();
            base.OnAppearing();
        }

        private async void GetLikesForRestaurant()
        {
            HttpResponseMessage response = await restoraniService.GetResponse("countRestaurantLikes", restoran.RestoranID.ToString());

            if(response.IsSuccessStatusCode)
            {
                likedByCount.Text = "Liked by: " + JsonConvert.DeserializeObject<int>(response.Content.ReadAsStringAsync().Result).ToString();
            }
        }

        private void RedirectToKomentari_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RestoranKomentari((Restorani_Result)restoran));
        }

        private void RedirectToJelovnik_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RestoranMenu((Restorani_Result)restoran));
        }

       
    }
}