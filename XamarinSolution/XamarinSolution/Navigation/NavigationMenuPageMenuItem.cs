﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinSolution.Navigation
{

    public class NavigationMenuPageMenuItem
    {
        public NavigationMenuPageMenuItem()
        {
            TargetType = typeof(RestoraniPage);
        }
        public string ImageSource { get; set; }
        public string Title { get; set; }

        public Type TargetType { get; set; }
    }
}