﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinSolution.Navigation
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NavigationMenuPageMaster : ContentPage
    {
        public ListView ListView;

        public NavigationMenuPageMaster()
        {
            InitializeComponent();

            BindingContext = new NavigationMenuPageMasterViewModel();
            ListView = MenuItemsListView;
        }

        class NavigationMenuPageMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<NavigationMenuPageMenuItem> MenuItems { get; set; }
            
            public NavigationMenuPageMasterViewModel()
            {
                MenuItems = new ObservableCollection<NavigationMenuPageMenuItem>(new[]
                {
                    new NavigationMenuPageMenuItem { ImageSource = "logo.png", Title = "Prijava", TargetType = typeof(Login) },
                    new NavigationMenuPageMenuItem { ImageSource = "logo.png", Title = "Registracija", TargetType = typeof(Registracija) },
                    new NavigationMenuPageMenuItem { ImageSource = "logo.png", Title = "Restorani", TargetType = typeof(RestoraniPage) }
                });
            }
            
            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}