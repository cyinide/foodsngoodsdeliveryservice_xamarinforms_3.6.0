﻿using FoodsNGoodsPCL.Model;
using FoodsNGoodsPCL.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinSolution
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        WebAPIHelper korisniciService = new WebAPIHelper("Auth");
        WebAPIHelper restoraniService = new WebAPIHelper("Restorani");
        public Login()
        {
            InitializeComponent();
        }

        private void BtnRegistracija_Clicked(object sender, EventArgs e)
        {
            Registracija regPage = new Registracija();
            NavigationPage.SetHasBackButton(regPage, false);
            Navigation.PushAsync(regPage);
        }

        private async void BtnLogin_Clicked(object sender, EventArgs e)
        {
            activityIndicator.IsRunning = true;
            Narucioci_Result narucilacObject = await Task<Narucioci_Result>.Run(async () =>
            {
                try
                {
                    HttpResponseMessage response = await korisniciService.GetResponse(entryUsername.Text);
                    if (response.IsSuccessStatusCode)
                    {
                        Narucioci_Result narucilac = JsonConvert.DeserializeObject<Narucioci_Result>(response.Content.ReadAsStringAsync().Result);
                        if (narucilac != null && narucilac.LozinkaHash == UIHelper.GenerateHash(narucilac.LozinkaSalt, entryPassword.Text))
                        {
                            HttpResponseMessage omiljeniRestoraniResponse = await restoraniService.GetResponse("favoriteRestorani", narucilac.KorisnikID.ToString());
                            if (omiljeniRestoraniResponse.IsSuccessStatusCode)
                            {
                                Global.logiraniNarucilacOmiljeniRestorani = JsonConvert.DeserializeObject<List<Restorani_Result>>(omiljeniRestoraniResponse.Content.ReadAsStringAsync().Result);
                            }

                            Global.logiraniNarucilac = narucilac;
                            return narucilac;
                        }
                    }
                    return null;
                }
                catch (Exception)
                {
                    return null;
                }
            });

            activityIndicator.IsRunning = false;

            if (narucilacObject == null)
            {
                await DisplayAlert("Invalid data", "Incorrect username or password", "OK");
            }
            else
            {
                await DisplayAlert("Successful login", "Logged in successfully", "OK");
                await Navigation.PushAsync(new RestoraniPage());
            }












            //Narucioci narucioc = await Task<Narucioci>.Run(() =>
            //{
            //    try
            //    {
            //        return null;
            //    }
            //    catch (Exception)
            //    {

            //        return null;
            //    }
            //});

        }
    }
}