﻿using FoodsNGoodsPCL.Model;
using FoodsNGoodsPCL.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinSolution
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Registracija : ContentPage
	{
        WebAPIHelper korisniciService = new WebAPIHelper("Auth");
        WebAPIHelper blokoviService = new WebAPIHelper("Blokovi");

        public List<Blokovi_Result> blokovi { get; set; }
        public Registracija ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {     
            LoadBlokovi();
            base.OnAppearing();
        }

       

        private async void LoadBlokovi()
        {
            activityIndicator.IsRunning = true;
            HttpResponseMessage response = await blokoviService.GetResponse();
            if (response.IsSuccessStatusCode)
            {
                blokovi = JsonConvert.DeserializeObject<List<Blokovi_Result>>(response.Content.ReadAsStringAsync().Result);
                pickerLocation.ItemsSource = blokovi;
                pickerLocation.ItemDisplayBinding = new Binding("BlokGradNaziv");
                pickerLocation.SelectedIndex = -1;
            }
            activityIndicator.IsRunning = false;
        }

        private async void BtnRegistracija_Clicked(object sender, EventArgs e)
        {
            Narucioci_Result narucilac = new Narucioci_Result
            {
                Ime = entryFirstname.Text,
                Prezime = entryLastname.Text,
                Username = entryUsername.Text,
                BlokID = ((Blokovi_Result)pickerLocation.SelectedItem).BlokID,
                DatumKreiranja = DateTime.Now,
                BadgeID =1
            };

            narucilac.LozinkaSalt = UIHelper.GenerateSalt();
            narucilac.LozinkaHash = UIHelper.GenerateHash(narucilac.LozinkaSalt, entryPassword.Text);

            HttpResponseMessage response = await korisniciService.PostResponse(narucilac);

            if(response.IsSuccessStatusCode)
            {
                await DisplayAlert("Successful registration", "Registered succesfully", "OK");
                Global.logiraniNarucilac = narucilac;
                await Navigation.PushAsync(new RestoraniPage());
            }


        }

        private void BtnToLogin_Clicked(object sender, EventArgs e)
        {
            Login loginPage = new Login();
            NavigationPage.SetHasBackButton(loginPage, false);
            Navigation.PushAsync(loginPage);
        }
    }
}