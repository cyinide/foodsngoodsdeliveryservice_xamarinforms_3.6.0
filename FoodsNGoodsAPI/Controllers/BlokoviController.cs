﻿using FoodsNGoodsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FoodsNGoodsAPI.Controllers
{
    [RoutePrefix("api/Blokovi")]
    [Route("")]
    public class BlokoviController : ApiController
    {
        FoodsNGoodsEntities context = new FoodsNGoodsEntities();

        [HttpGet]
        public IHttpActionResult GetBlokovi()
        {
            List<Blokovi> blokovi = context.Blokovi.ToList();

            if(blokovi != null)
            {
                return Ok(Blokovi_Result.GetBlokoviResultInstance(blokovi));
            }

            return NotFound();
        }
    }
}
