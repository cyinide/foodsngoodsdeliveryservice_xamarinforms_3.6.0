﻿using FoodsNGoodsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FoodsNGoodsAPI.Controllers
{
    [RoutePrefix("api/Proizvodi")]
    [Route("")]
    public class ProizvodiController : ApiController
    {
        FoodsNGoodsEntities context = new FoodsNGoodsEntities();

        [Route("{restoranId}/GetProizvodiByRestoran")]
        [HttpGet]
        public IHttpActionResult GetProizvodiByRestoran([FromUri]int restoranId)
        {
            List<Proizvodi> proizvodi = context.Proizvodi.Where(x => x.Jelovnici.RestoranID == restoranId).ToList();

            return Ok(Proizvodi_Result.GetProizvodiResultInstance(proizvodi));

        }
    }
}
