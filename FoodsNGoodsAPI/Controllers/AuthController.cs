﻿using FoodsNGoodsAPI.Models;
using System.Linq;
using System.Web.Http;


namespace FoodsNGoodsAPI.Controllers
{
    [RoutePrefix("api/Auth")]
    [Route("")]
    public class AuthController : ApiController
    {
        FoodsNGoodsEntities context = new FoodsNGoodsEntities();

        [HttpGet]
        [Route("{username}")]
        public IHttpActionResult Login([FromUri]string username)
        {
            Narucioci narucilac = context.Narucioci.Where(x => x.Username == username).FirstOrDefault();

            if (narucilac != null)
            {
                return Ok(Narucioci_Result.GetNaruciociResultInstance(narucilac));
            }

            return NotFound();
        }

        [HttpPost]
        public IHttpActionResult Register([FromBody] Narucioci obj)
        {
            if(obj == null)
            {
                return BadRequest();
            }

            context.Narucioci.Add(obj);
            context.SaveChanges();

            return Ok(obj);
        }
    }
}
