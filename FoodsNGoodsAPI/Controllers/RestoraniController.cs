﻿    using FoodsNGoodsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace FoodsNGoodsAPI.Controllers
{
    [RoutePrefix("api/Restorani")]
    [Route("")]
    public class RestoraniController : ApiController
    {
        FoodsNGoodsEntities context = new FoodsNGoodsEntities();

        [HttpGet]
        public IHttpActionResult GetRestorani()
        {
            List<Restorani> restorani = context.Restorani.ToList();

            if (restorani == null)
            {
                return NotFound();
            }

            return Ok(Restorani_Result.GetRestoraniResultInstance(restorani));
        }

        [Route("{korisnikId}/favoriteRestorani")]
        [HttpGet]
        public IHttpActionResult GetFavourite(int korisnikId)
        {
            List<Restorani> restorani = context.RestoraniOmiljeni.Where(X => X.KorisnikID == korisnikId).Select(x=>x.Restorani).ToList();

            return Ok(Restorani_Result.GetRestoraniResultInstance(restorani));
        }

        [Route("{restoranId}/countRestaurantLikes")]
        [HttpGet]
        public IHttpActionResult CountRestaurantLikes(int restoranId)
        {
            int restoraniLikesCount = context.RestoraniOmiljeni.Where(X => X.RestoranID == restoranId).Count();

            return Ok(restoraniLikesCount);
        }

        [Route("{korisnikId}/toggleLikeRestoran")]
        [HttpPost]
        public IHttpActionResult ToggleLikeRestoran([FromUri]int korisnikId,[FromBody]int restoranId)
        {
            restoranId = 2;
            RestoraniOmiljeni restoranLike = context.RestoraniOmiljeni.Where(x => x.KorisnikID == korisnikId && x.RestoranID == restoranId).FirstOrDefault();

            if(restoranLike == null)
            {
                context.RestoraniOmiljeni.Add(new RestoraniOmiljeni { KorisnikID = korisnikId, RestoranID = restoranId });             
            }
            else
            {
                context.RestoraniOmiljeni.Remove(restoranLike);           
            }
            context.SaveChanges();
            return Ok();
        }


    }
}
