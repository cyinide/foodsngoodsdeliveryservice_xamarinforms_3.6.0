﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodsNGoodsAPI.Models
{
    public class Proizvodi_Result
    {
        public int HranaID { get; set; }
        public double Cijena { get; set; }
        public int JelovnikID { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public string Prilog { get; set; }
        public int Sifra { get; set; }
        public string Slika { get; set; }
        public int TipKuhinjeID { get; set; }


        public static Proizvodi_Result GetProizvodiResultInstance(Proizvodi obj)
        {
            return obj == null ? null : new Proizvodi_Result
            {
                HranaID = obj.HranaID,
                Cijena = obj.Cijena,
                JelovnikID = obj.JelovnikID,
                Naziv = obj.Naziv,
                Opis = obj.Opis,
                Prilog = obj.Prilog,
                Sifra = obj.Sifra,
                Slika = obj.Slika,
                TipKuhinjeID = obj.TipKuhinjeID
            };

        }

        public static List<Proizvodi_Result> GetProizvodiResultInstance(List<Proizvodi> obj)
        {
            List<Proizvodi_Result> restoraniList = new List<Proizvodi_Result>();
            foreach (var x in obj)
            {
                restoraniList.Add(Proizvodi_Result.GetProizvodiResultInstance(x));
            }
            return obj == null ? null : restoraniList;
        }
    }
}