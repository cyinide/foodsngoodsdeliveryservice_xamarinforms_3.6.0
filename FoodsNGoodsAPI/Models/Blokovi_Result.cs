﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodsNGoodsAPI.Models
{
    public class Blokovi_Result
    {
        public int BlokID { get; set; }
        public int GradID { get; set; }
        public string Naziv { get; set; }
        public string BlokGradNaziv { get; set; }


        public static Blokovi_Result GetBlokoviResultInstance(Blokovi obj)
        {
            return obj == null ? null : new Blokovi_Result
            {
                BlokID = obj.BlokID,
                GradID = obj.GradID,
                BlokGradNaziv = obj.Naziv + ", " + obj.Gradovi.Naziv
            };
        }

        public static List<Blokovi_Result> GetBlokoviResultInstance(List<Blokovi> obj)
        {
            List<Blokovi_Result> blokoviList = new List<Blokovi_Result>();
            foreach (var x in obj)
            {
                blokoviList.Add(Blokovi_Result.GetBlokoviResultInstance(x));
            }
            return obj == null ? null : blokoviList;
        }
    }
}