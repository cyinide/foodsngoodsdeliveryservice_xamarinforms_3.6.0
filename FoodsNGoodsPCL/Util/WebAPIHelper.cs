﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FoodsNGoodsPCL.Util
{
    public class WebAPIHelper
    {
        private HttpClient client { get; set; }
        private string route { get; set; }
        public static string API_PREFIX = "api";
        public static string API_BASE = "http://192.168.0.13";

        public WebAPIHelper(string route)
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(API_BASE);
            this.route = API_PREFIX + "/" + route;

        }
        public async Task<HttpResponseMessage> GetResponse(string parameter = "")
        {
            HttpResponseMessage result = await client.GetAsync(route + "/" + parameter);
            return result;

        }

        public async Task<HttpResponseMessage> GetResponse(string additionalAction, string parameter = "")
        {
            HttpResponseMessage result = await client.GetAsync(route + "/" + parameter + "/" + additionalAction);
            return result;

        }

        public HttpResponseMessage GetActionResponse(string action, string parameter = "")
        {
            return client.GetAsync(route + "/" + action + "/" + parameter).Result;
        }

        public async Task<HttpResponseMessage> PostResponse(object newObject)
        {
            var jsonObject = new StringContent(JsonConvert.SerializeObject(newObject), Encoding.UTF8, "application/json");
            return await client.PostAsync(route, jsonObject);
        }

        public async Task<HttpResponseMessage> PostResponse(string parameter, object newObject)
        {
            var jsonObject = new StringContent(JsonConvert.SerializeObject(newObject), Encoding.UTF8, "application/json");
            return await client.PostAsync(route + "/" + parameter + "/", jsonObject);
        }

        //public HttpResponseMessage PostActionResponse(string action, Object newObject)
        //{
        //    return client.PostAsJsonAsync(route + "/" + action, newObject).Result;
        //}

        //public HttpResponseMessage PutResponse(int id, Object existingObject)
        //{
        //    return client.PutAsJsonAsync(route + "/" + id, existingObject).Result;
        //}
    }
}
