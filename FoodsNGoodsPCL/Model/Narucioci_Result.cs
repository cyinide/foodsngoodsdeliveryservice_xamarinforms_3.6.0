﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodsNGoodsPCL.Model
{
   public class Narucioci_Result
    {
        public int KorisnikID { get; set; }
        public int BadgeID { get; set; }
        public int BlokID { get; set; }
        public Nullable<System.DateTime> DatumKreiranja { get; set; }
        public string Email { get; set; }
        public string Ime { get; set; }
        public string Password { get; set; }
        public string Prezime { get; set; }
        public string Telefon { get; set; }
        public string Username { get; set; }
        public string ImageUrl { get; set; }
        public string Adresa { get; set; }
        public string LozinkaHash { get; set; }
        public string LozinkaSalt { get; set; }
    }
}
