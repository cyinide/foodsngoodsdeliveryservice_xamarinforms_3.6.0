﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodsNGoodsPCL.Model
{
   public class Restorani_Result
    {
        public int RestoranID { get; set; }
        public int BlokID { get; set; }
        public int MinimalnaCijenaNarudžbe { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public string Slika { get; set; }
        public string Slogan { get; set; }
        public string Telefon { get; set; }
        public int VlasnikID { get; set; }
        public string WebUrl { get; set; }
        public string Adresa { get; set; }
        public string Email { get; set; }
        public string BlokGradNaziv { get; set; }
        public string VlasnikNaziv { get; set; }
        public string AdresaFullNaziv { get; set; }
        public byte[] SlikaBin { get; set; }
    }
}
