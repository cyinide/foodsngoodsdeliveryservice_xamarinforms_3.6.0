﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FoodsNGoodsPCL.Model
{
    public class Blokovi_Result
    {
        public int BlokID { get; set; }
        public int GradID { get; set; }
        public string Naziv { get; set; }
        public string BlokGradNaziv { get; set; }
    }
}