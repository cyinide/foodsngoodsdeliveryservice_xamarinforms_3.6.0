﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodsNGoodsPCL.Model
{
    public class Proizvodi_Result
    {
        public int HranaID { get; set; }
        public double Cijena { get; set; }
        public int JelovnikID { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public string Prilog { get; set; }
        public int Sifra { get; set; }
        public string Slika { get; set; }
        public int TipKuhinjeID { get; set; }
    }
}
